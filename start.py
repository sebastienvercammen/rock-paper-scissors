import argparse
from random import randint
import time

from pick import Picker

from rps.constants import (
    RPS_CHOICES,
    SCORE_LOG_SEPARATOR,
    RESULTS_LOG_FILENAME
)
from rps.prints import (
    please_press_enter,
    print_rules,
    print_score_tables,
    show_history
)
from rps.rules import determine_winner, rps_distance
from rps.types import Choices, Players
from rps.vendor.pick import attach_rps_keyboard_shortcuts


def get_args():
    parser = argparse.ArgumentParser(description='RPS')
    parser.add_argument(
        '--history',
        help='Show full history of all past games + the score totals.',
        action='store_true'
    )
    return parser.parse_args()


def game():
    # Show game rules on startup.
    print_rules()
    please_press_enter()

    # Set up the pick library.
    input_title, input_options = (
        'Rock, Paper, Scissors... or quit',
        [choice.value for choice in Choices]
    )
    picker = Picker(input_options, input_title)

    # Optional: Map keyboard shortcuts to their original entry.
    # Added for usability. Not a requirement.
    attach_rps_keyboard_shortcuts(picker)

    # Game loop.
    rps = ''
    scores = {
        Players.COMPUTER: 0,
        Players.PLAYER: 0,
        'Tie': 0
    }

    # Results log to append to. buffering=1 is line-buffered:
    # https://docs.python.org/3/library/functions.html#open
    with open(RESULTS_LOG_FILENAME, 'a', buffering=1, encoding='UTF-8') as fh:
        # Game ends on [Q]uit.
        while rps != Choices.QUIT:
            rps, _ = picker.start()

            # str to Enum
            rps = Choices(rps)

            # Print scores and break early on exit.
            if rps == Choices.QUIT:
                print('\n\n=============================\n')
                print('Game is over. Scores this round:\n')
                print_score_tables(scores)
                break

            # Computer makes a choice.
            computer_rps = RPS_CHOICES[randint(0, len(RPS_CHOICES) - 1)]

            # Game rules:
            #   RPS is ordered: R < P < S, with a loopback of S < R.
            #   With the 1-indexed list: R=1, P=2, S=3.
            #   When ordered, the winner can be determined based on distance:
            #       distance = t(highest index, lowest index)
            #   with the rules:
            #       * If distance = -2, first player (player) wins
            #       * If distance = -1, first player (player) wins
            #       * If distance = 0, it's a tie
            #       * If distance = 1, second player (computer) wins
            #       * If distance = 2, it's Rock vs Scissors and
            distance = rps_distance(computer_rps, rps)
            winner = determine_winner(distance)
            result_text = 'Tie' if winner is None else f'{winner} wins'

            # Increase score.
            scores[winner or 'Tie'] += 1

            # Append line to result log.
            fh.write(
                SCORE_LOG_SEPARATOR.join(
                    [str(time.time()), computer_rps, rps]
                ) + '\n'
            )

            # Print round result.
            print(f'\n\nPlayer picked:\t\t{rps}')
            print(f'Computer picked:\t{computer_rps}')
            print(f'---> {result_text}')
            please_press_enter()


if __name__ == '__main__':
    args = get_args()

    if not args.history:
        game()
    else:
        show_history()
