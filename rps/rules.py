from rps.constants import RPS_CHOICES, WINNER_MAP
from rps.types import Choices, Players


def rps_distance(player: Choices, computer: Choices):
    return RPS_CHOICES.index(computer) - RPS_CHOICES.index(player)


def determine_winner(distance: int):
    return WINNER_MAP[distance]
