from pick import Picker


def attach_rps_keyboard_shortcuts(picker):
    def rock(picker: Picker):
        return 'rock', 0

    def paper(picker: Picker):
        return 'paper', 1

    def scissors(picker: Picker):
        return 'scissors', 2

    def quit(picker: Picker):
        return 'quit', 3

    picker.register_custom_handler(ord('r'), rock)
    picker.register_custom_handler(ord('p'), paper)
    picker.register_custom_handler(ord('s'), scissors)
    picker.register_custom_handler(ord('q'), quit)
