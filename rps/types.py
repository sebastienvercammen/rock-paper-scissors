from enum import Enum


class Choices(str, Enum):
    ROCK = 'rock'
    PAPER = 'paper'
    SCISSORS = 'scissors'
    QUIT = 'quit'


class Players(str, Enum):
    COMPUTER = 'Computer'
    PLAYER = 'Player'
