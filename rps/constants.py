from typing import Dict, Optional

from rps.types import Choices, Players


SCORE_LOG_SEPARATOR = '|'
RESULTS_LOG_FILENAME = 'results.log'
# For randomization, we make the fixed, ordered list of computer options once.
# We don't enumerate the enum because earlier Python versions don't
# guarantee member order.
RPS_CHOICES = [
    Choices.ROCK,
    Choices.PAPER,
    Choices.SCISSORS
]

WINNER_MAP: Dict[int, Optional[Players]] = {
    -2: Players.COMPUTER,  # Rock (1, computer) - Scissors (3, player)
    -1: Players.PLAYER,
    0: None,
    1: Players.COMPUTER,
    2: Players.PLAYER      # Scissors (3, computer) - Rock (1, player)
}
