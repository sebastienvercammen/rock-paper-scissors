from datetime import datetime
import os

from tabulate import tabulate

from rps.constants import RESULTS_LOG_FILENAME, SCORE_LOG_SEPARATOR
from rps.rules import determine_winner, rps_distance
from rps.types import Players


def print_rules():
    print("""Rock/Paper/Scissors game rules:
Rock vs paper -> paper wins
Rock vs scissors -> rock wins
Paper vs scissors -> scissors wins

You can use keyboard shortcuts: [R]ock, [P]aper, [S]cissors, [Q]uit.""")


def please_press_enter():
    input('\nPress Enter to continue...')


def tabulate_scores(scores):
    # StrEnum converts to its value in interpolation, while str 'Tie' is
    # taken as-is.
    return tabulate(
        [
            [f'{player}', scores[player]]
            for player in scores
        ],
        headers=['Name', 'Score']
    )


def print_score_tables(scores):
    print(tabulate_scores(scores))


def history_generator(log_filename):
    with open(log_filename, 'r', encoding='UTF-8') as fh:
        for line in fh:
            yield line.strip().split(SCORE_LOG_SEPARATOR)


def show_history():
    if not os.path.isfile(RESULTS_LOG_FILENAME):
        print('No saved results were found.')
        return

    scores = {
        Players.COMPUTER: 0,
        Players.PLAYER: 0,
        'Tie': 0
    }

    print('\nFull history of RPS:\n')
    print('%-30s%-12s%-12s%-12s' % ('TIME', 'COMPUTER', 'PLAYER', 'WINNER'))

    # Print history line-by-line to limit memory consumption.
    for timestamp, computer, player in history_generator(RESULTS_LOG_FILENAME):
        date = datetime.fromtimestamp(float(timestamp))
        winner = determine_winner(rps_distance(computer, player))
        result_text = 'Tie' if winner is None else winner.value

        # Increase score.
        scores[winner or 'Tie'] += 1

        print(
            '%-30s%-12s%-12s%-12s'
            % (date, computer, player, result_text)
        )

    # Print score aggregates.
    print('\n\n=============================\n')
    print('Total scores:\n')
    print_score_tables(scores)
